#include <Arduino.h>
#include <SPI.h>
#include <MFRC522v2.h>
#include <MFRC522DriverSPI.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Debug.h>
#include "config.h"
#include <stdint.h>

#define PIN_CS 10  // Chip-select pin for the MFRC522 module
#define PIN_CARD_PRESENT 2
#define PIN_CARD_CORRECT 3
#define OUT_ACTIVE LOW
#define BAUD_RATE 57600

// Here only UID will be compared and SAK ignored
static bool isUidEqual(const MFRC522::Uid &uid1, const MFRC522::Uid &uid2) {
  size_t size = uid1.size;
  return (
    size == uid2.size
    && !memcmp(uid1.uidByte, uid2.uidByte, size));
}

// str needs to have a size of at least 30
static void uidToStr(char str[], const MFRC522::Uid &uid) {
  char *iter = str;
  *iter = '\0';
  for (int i = 0; i < uid.size; i++) {
    if (i) {
      *iter = ' ';
      iter++;
    }
    iter += sprintf(iter, "%02x", uid.uidByte[i]);
  }
}

static bool isCardPresent(MFRC522 &mfrc) {
  return mfrc.PICC_ReadCardSerial() || (mfrc.PICC_IsNewCardPresent() && mfrc.PICC_ReadCardSerial());
}

static const SPISettings spiSettings = SPISettings(500000, MSBFIRST, SPI_MODE0);  // Maximum 500 kHz
static MFRC522DriverPinSimple mfrcDriverPin = PIN_CS;
static MFRC522DriverSPI mfrcDriver(mfrcDriverPin, SPI, spiSettings);
static MFRC522 mfrc(mfrcDriver);

void setup() {
  Serial.begin(BAUD_RATE);  // Initialize serial communication
  SPI.begin();  // Init SPI bus
  while (!Serial)
    ;

  Serial.println("Start of RFID Access Arduino Program");
  Serial.print("Serial baud-rate: ");
  Serial.println(BAUD_RATE);
  Serial.print("Chip-select pin for MFRC522: ");
  Serial.println(PIN_CS);
  Serial.print("Card-present output pin: ");
  Serial.println(PIN_CARD_PRESENT);
  Serial.print("Card-correct output pin: ");
  Serial.println(PIN_CARD_CORRECT);
  Serial.print("The output pins are active-");
  Serial.print(OUT_ACTIVE == HIGH ? "high" : "low");
  Serial.println(".");
  Serial.println("The following Card UIDs are correct:");
  for (const MFRC522::Uid *iter = correctUids; iter->size; iter++) {
    char buf[32];
    uidToStr(buf, *iter);
    Serial.print("- ");
    Serial.println(buf);
  }
  Serial.println();

  digitalWrite(PIN_CARD_PRESENT, !OUT_ACTIVE);
  pinMode(PIN_CARD_PRESENT, OUTPUT);
  digitalWrite(PIN_CARD_CORRECT, !OUT_ACTIVE);
  pinMode(PIN_CARD_CORRECT, OUTPUT);

  Serial.print("Initializing MFRC522...");
  while (!mfrc.PCD_Init()) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println(" Done.");  
  delay(10);
  MFRC522Debug::PCD_DumpVersionToSerial(mfrc, Serial);
  Serial.println();
}

void loop() {
  // Between the write to the present and correct pin it can take a few instruction cycles.
  // So there could be a short period of time where present and correct pin are not synchronized.
  // For example, when there was no card first, and then the correct card was tapped,
  // first the present pin becomes low and afterwards the correct pin.
  // This may result in a short spurious "present but not correct" message on the peer Arduino,
  // but this should not be big issue.

  Serial.print("Initializing MFRC522...");
  while (!mfrc.PCD_Init()) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println(" Done.");

  if (isCardPresent(mfrc)) {
    Serial.print("Card present: ");
    char buf[32];
    uidToStr(buf, mfrc.uid);
    Serial.print(buf);

    bool correct = false;
    const MFRC522::Uid *iter = correctUids;
    for (const MFRC522::Uid *iter = correctUids; iter->size; iter++) {
      if (isUidEqual(mfrc.uid, *iter)) {
        correct = true;
        break;
      }
    }
    if (correct) {
      Serial.println(" -> correct");
    } else {
      Serial.println(" -> wrong");
    }

    digitalWrite(PIN_CARD_PRESENT, OUT_ACTIVE);
    if (correct) {
      digitalWrite(PIN_CARD_CORRECT, OUT_ACTIVE);
    } else {
      digitalWrite(PIN_CARD_CORRECT, !OUT_ACTIVE);
    }

  } else {
    Serial.println("No card");
    digitalWrite(PIN_CARD_CORRECT, !OUT_ACTIVE);
    digitalWrite(PIN_CARD_PRESENT, !OUT_ACTIVE);
  }
  delay(50);
}
