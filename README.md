# RFID Access

Arduino sketch which reads RFID cards using the MFRC522. The Arduino indicates with output pins, whether a card is present and whether it is a correct card.

The UIDs of the correct cards can be defined in a custom file `config.cpp`.
